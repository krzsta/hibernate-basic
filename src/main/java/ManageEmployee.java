import model.Note;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class ManageEmployee {
    private static String config = "hibernate.cfg.xml";

    public static void main(String[] args) {
        Note note = new Note();
        note.setContent("nowa notatka hibernate");
        note.setTitle("tytul notatki");
        note.setAuthor("Krzysiek");

        addToDatabase(note, config);

        Note object = (Note) selectFromDatabase(10, config, Note.class);
        object.setAuthor("Marian");

        updateDatabase(object, config);
    }

    public static SessionFactory getSessionFactory(String config) {
        return new Configuration().configure(config).buildSessionFactory();
    }

    public static void addToDatabase(Object object, String config) {
        Session session = getSessionFactory(config).openSession();
        session.beginTransaction();
        session.save(object);
        session.getTransaction().commit();
        session.close();
    }

    public static void updateDatabase(Object object, String config) {
        Session session = getSessionFactory(config).openSession();
        session.beginTransaction();
        session.update(object);
        session.getTransaction().commit();
        session.close();
    }

    public static Object selectFromDatabase(long id, String config, Class clas) {
        Session session = getSessionFactory(config).openSession();
        session.beginTransaction();
        Object object = session.get(clas, id);
        session.getTransaction().commit();
        session.close();

        return object;
    }
}
